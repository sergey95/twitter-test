# README #

Use this skeleton page to implement a simple twitter integration. In the marked area on the page, display the latest tweet from the @core_dev_ twitter account. When you enter a new tweet into the box, send a new tweet via the @core_dev_ account. The `.env` file includes the credentials for the account which you will need for sending a tweet.

Work as you normally would, but don't spend more than an hour on the project if you can avoid it. If you cut corners to finish, say what you would have done differently in a production environment, and what improvements could be made.

## Instructions for submission:

Fork this repo and create a new branch, you may call it whatever you want. When complete, submit a pull request to the repo and email office@coredatasystems.co.uk to let us now you've completed the task.